# AUTHORS

## qrcode.min.js
* MIT License
* Copyright (c) 2012 davidshimjs
* https://github.com/davidshimjs/qrcodejs

## vue.min.js
* MIT License
* Copyright (c) 2013-present, Yuxi (Evan) You
* Vue.js: https://github.com/vuejs/vue

## c3.min.js
* MIT License
* Copyright (c) 2013 Masayuki Tanaka
* https://github.com/c3js/c3

## d3.v5.min.js
* BSD 3-Clause License
* Copyright 2010-2017 Mike Bostock
* https://github.com/d3/d3

## moment.js
* MIT License
* Copyright (c) JS Foundation and other contributors
* https://github.com/moment/moment

## Hamburger_icon.svg
* CC-BY-SA 3.0
* Timothy Miller
* https://www.iconfinder.com/tmthymllr

## instascan.min.js
* MIT License
* Copyright (c) 2016 Chris Schmich
* https://github.com/schmich/instascan
