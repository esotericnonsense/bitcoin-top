# bitcoin-top
Bitcoin Core node monitoring service.

https://bitcoin.esotericnonsense.com

# NOTE: please use tags (v0.1.0) for deployment on the real internet.
#   the master branch is _not_ secure and is only for use on testnet
#   you have been warned.

## Licence
![ScreenShot](/nginx/public_html/agplv3-155x51.png)
bitcoin-top is Free Software, licensed under the [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html).

## Dependencies
* docker, docker-compose
* Developed with: Rust, PostgreSQL, JavaScript, HTML, CSS, bash...

## Features
* Persistent database of historical memory pool data and network activity
* Live display of blocktemplate
* Live display of block information
* Node uptime
* To be continued...

## Installation and usage
* Ensure that the dependencies (docker, docker-compose) are installed on your
    system.
* If you would like to use your own postgres or bitcoind instance, edit
    genconfig.sh as necessary.
* bash genconfig.sh -> to generate docker-compose.yml
* bash run.sh -> to start the containers

* By default the service will listen on port 3000.
* During initial synchronization the interface may be slow. This is a work
    in progress.

## Feedback
Please report any problems to bitcoin@esotericnonsense.com.

Patches welcomed.

The author, Daniel Edgecumbe (esotericnonsense) can often be found milling
around on IRC (#bitcoin, #bitcoin-core-dev, #bitcoin-dev Freenode).

## Thanks
Special credit to the Bitcoin Core development team who work tirelessly to
ensure that the network continues to spin.

## Donations
Development work on bitcoin-top is wholly funded by donation.

![ScreenShot](/nginx/public_html/3BYFucUnVNhZjUDf6tZweuZ5r9PPjPEcRv.png)

**bitcoin 3BYFucUnVNhZjUDf6tZweuZ5r9PPjPEcRv**
