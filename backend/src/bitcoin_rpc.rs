//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate serde_json;

use hyper::{Method, Body, Request, Client};
use hyper::client::ResponseFuture;
use hyper::header::{HeaderName, HeaderValue};

#[derive(Serialize, Deserialize, Debug)]
#[serde(untagged)]
pub enum BitcoinRPCParameter {
    Bool(bool),
    Int(i64),
    Str(String),
    Any(serde_json::Value),
}

#[derive(Serialize, Deserialize, Debug)]
struct BitcoinRPCRequest {
    jsonrpc: String,
    method: String,
    params: Vec<BitcoinRPCParameter>,
    id: isize,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct BitcoinRPCResponse {
    result: serde_json::Value, // this is lazy
    error: serde_json::Value,
    id: serde_json::Value,
}

fn craft_req(method: String, params: Vec<BitcoinRPCParameter>) -> BitcoinRPCRequest {
    return BitcoinRPCRequest {
        jsonrpc: "2.0".to_string(),
        method: method,
        params: params,
        id: 0, // TODO choose uuid
    }
}

pub fn get_resp_future_rpc(method: String, params: Option<Vec<BitcoinRPCParameter>>, rpc_auth: &str, rpc_addr: &str) -> ResponseFuture {
    let uri: hyper::Uri = format!("http://{}", rpc_addr)
        .parse().unwrap();

    let real_params = match params {
        Some(x) => x,
        None => Vec::<BitcoinRPCParameter>::new(),
    };

    let unser = craft_req(method, real_params);
    let j_serialized = serde_json::to_string(&unser).unwrap();

    let au = HeaderName::from_lowercase(b"authorization").unwrap();
    let au2 = HeaderValue::from_str(&format!("Basic {}", rpc_auth)).unwrap();

    let ct = HeaderName::from_lowercase(b"content-type").unwrap();
    let ct2 = HeaderValue::from_static("text/plain");

    let mut req = Request::new(Body::from(j_serialized));
    *req.method_mut() = Method::POST;
    *req.uri_mut() = uri.clone();
    req.headers_mut().insert(au, au2);
    req.headers_mut().insert(ct, ct2);

    Client::new().request(req)
}

pub fn get_resp_future_rest(relpath: String, rpc_addr: &str) -> ResponseFuture {
    let raw_uri = format!("http://{}{}", rpc_addr, relpath);
    let uri: hyper::Uri = raw_uri.parse().unwrap();

    Client::new().get(uri)
}
