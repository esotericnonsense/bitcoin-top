//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate base64;
extern crate futures;
extern crate hyper;
use hyper::{Method, Request, StatusCode, Server, Body, Response};
use hyper::rt::{Future};
use hyper::service::service_fn;
#[macro_use]
extern crate lazy_static;
extern crate postgres;
extern crate r2d2;
extern crate r2d2_postgres;
use r2d2_postgres::{TlsMode, PostgresConnectionManager};
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

mod log;
use log::log;
mod server_get;
pub mod bitcoin_rpc;
mod persistence;

use std::thread::spawn;

type BoxFut = Box<dyn Future<Item=Response<Body>, Error=hyper::Error> + Send>;
type PgPool = r2d2::Pool<PostgresConnectionManager>;

fn server(mainpool: PgPool, config: Config) -> Box<dyn Future<Item=(), Error=()> + Send> {
    const NOTFOUND: &str = "404 Not Found";

    fn respond(req: Request<Body>, pool: &PgPool, rpc_auth: &str, rpc_addr: &str) -> BoxFut {
        match (req.method(), req.uri().path()) {
            (&Method::GET, x) => {
                return server_get::server_get(x, pool, rpc_auth, rpc_addr)
            },
            _ => {
                let mut response = Response::new(Body::empty());
                *response.body_mut() = Body::from(NOTFOUND);
                *response.status_mut() = StatusCode::NOT_FOUND;
                return Box::new(futures::future::ok(response));
            }
        }
        // unreachable
    }

    let server = Server::bind(&config.listen)
        .serve(move || {
            let tpool = mainpool.clone();
            let rpc_auth = config.rpc_auth.to_owned();
            let rpc_addr = config.rpc_addr.to_owned();
            service_fn(move |req| {
                respond(req, &tpool, &rpc_auth, &rpc_addr)
            })
        })
        .map_err(|e| eprintln!("did a bad: {}", e));

    Box::new(server)
}

struct Config {
    listen: std::net::SocketAddr,
    rpc_auth: String,
    rpc_addr: String,
}

fn main() {
    let db_name = std::env::var("BM_DBNAME").expect("need BM_DBNAME env var");
    let db_role = std::env::var("BM_DBROLE").expect("need BM_DBROLE env var");
    let db_host = std::env::var("BM_DBHOST").expect("need BM_DBHOST env var");
    let db_pass = std::env::var("BM_DBPASS").expect("need BM_DBPASS env var");
    let db_uri = format!(
        "postgres://{}:{}@{}/{}",
        db_role, db_pass, db_host, db_name
    );

    let manager = PostgresConnectionManager::new(
        db_uri, TlsMode::None).unwrap();
    let pool = r2d2::Pool::new(manager).unwrap();

    let listen_raw = match std::env::var("BM_LISTEN") {
        Ok(x) => x,
        Err(_) => "0.0.0.0:80".to_string(),
    };
    let listen: std::net::SocketAddr = listen_raw.parse()
        .expect("could not parse listen address");

    let rpc_user = std::env::var("BM_RPCUSER").expect("need BM_RPCUSER env var");
    let rpc_pass = std::env::var("BM_RPCPASS").expect("need BM_RPCPASS env var");
    let rpc_auth = base64::encode(&format!("{}:{}", rpc_user, rpc_pass));

    let rpc_addr = std::env::var("BM_RPCADDR").expect("need BM_RPCADDR env var");

    let config = Config { listen, rpc_auth, rpc_addr };

    // Persistence thread
    let tpool = pool.clone();
    let rpc_auth = config.rpc_auth.to_owned();
    let rpc_addr = config.rpc_addr.to_owned();
    spawn(move || {
        persistence::persistence_thread(tpool, &rpc_auth, &rpc_addr)
    });

    // Server
    log(&format!("entered proxy mode on http://{}", listen));
    let s = server(pool, config);
    hyper::rt::run(s);
}
