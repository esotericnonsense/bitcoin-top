//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate futures;

use futures::{Future, Stream};

use std::thread::sleep;
use std::time::Duration;

use bitcoin_rpc;
use server_get::mempoolbins;
use server_get::nettotals;

use PgPool;

use log::log;

fn perform_mempoolbins_cycle(pool: PgPool, rpc_addr: &str) {
    // TODO: duplicate code here.
    let fut = bitcoin_rpc::get_resp_future_rest(
            "/rest/mempool/contents.json".to_string(),
            rpc_addr
        )
        .and_then(move |res| {
            res.into_body()
            .concat2()
            .map(move |chunk| {
                let (utc_ms, block) = mempoolbins::process_mempoolbins(chunk);

                let conn = match pool.get() {
                    Ok(c) => c,
                    Err(e) => {
                        log(&format!("{}", e));
                        return ()
                    },
                };

                let q = "INSERT INTO mempoolbins VALUES ($1, $2);";
                match conn.execute(q, &[&utc_ms, &block]) {
                    Ok(_) => (),
                    Err(e) => {
                        log(&format!("{}", e));
                    },
                };

            })
        }).map_err(|_| {});

    log("starting mempoolbins cycle");
    hyper::rt::run(fut);
    log("finished mempoolbins cycle");
}

fn perform_nettotals_cycle(pool: PgPool, rpc_auth: &str, rpc_addr: &str) {
    // TODO: duplicate code here.
    let fut = bitcoin_rpc::get_resp_future_rpc(
            "getnettotals".to_string(),
            None,
            rpc_auth,
            rpc_addr,
        )
        .and_then(move |res| {
            res.into_body()
            .concat2()
            .map(move |chunk| {
                let (utc_ms, block) = nettotals::process_nettotals(chunk);

                let conn = match pool.get() {
                    Ok(c) => c,
                    Err(e) => {
                        log(&format!("{}", e));
                        return ()
                    },
                };

                let q = "INSERT INTO nettotals VALUES ($1, $2);";
                match conn.execute(q, &[&utc_ms, &block]) {
                    Ok(_) => (),
                    Err(e) => {
                        log(&format!("{}", e));
                    },
                };
            })
        }).map_err(|_| {});

    log("starting nettotals cycle");
    hyper::rt::run(fut);
    log("finished nettotals cycle");
}

fn create_tables(pool: PgPool) {
    let conn = pool.get().unwrap();

    let queries = vec![
        "
        CREATE TABLE mempoolbins (
            utc_ms int8 PRIMARY KEY,
            json varchar
        );
        ",
        "
        CREATE INDEX utc_ms_index on mempoolbins(utc_ms);
        ",
        "
        CREATE TABLE nettotals (
            utc_ms int8 PRIMARY KEY,
            json varchar
        );
        ",
        "
        CREATE INDEX utc_ms_index_nettotals ON nettotals (utc_ms);
        "
    ];

    for q in queries {
        match conn.execute(q, &[]) {
            Ok(_) => (),
            Err(e) => {
                log(&format!("{}", e));
            },
        };
    }
}

pub fn persistence_thread(pool: PgPool, rpc_auth: &str, rpc_addr: &str) {
    create_tables(pool.clone());
    loop {
        perform_mempoolbins_cycle(pool.clone(), rpc_addr);
        perform_nettotals_cycle(pool.clone(), rpc_auth, rpc_addr);
        sleep(Duration::from_millis(15000));
    }
}
