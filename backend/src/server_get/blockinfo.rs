//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate serde_json;

use log::log;

#[derive(Serialize, Deserialize, Debug)]
struct RPCBlockTxVout {
    value: f64,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCBlockTx {
    vout: Vec<RPCBlockTxVout>,
    hash: String,
    txid: String,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCBlock {
    chainwork: String,
    difficulty: f64,
    hash: String,
    height: u32,
    time: u64,
    previousblockhash: Option<String>,
    size: u32,
    weight: u32,
    tx: Vec<RPCBlockTx>,
}

#[derive(Serialize, Deserialize, Debug)]
struct BlockinfoBlock {
    chainwork: String,
    difficulty: f64,
    hash: String,
    height: u32,
    time: u64,
    previousblockhash: String,
    size: u32,
    weight: u32,
    subsidy: f64,
    fees: f64,
    tx_count: u32,
    tx_count_segwit: u32,
}

fn block_subsidy(height: u32) -> f64 {
    let interval = 210000;
    let halvings = height / interval;
    if halvings >= 64 {
        return 0.;
    }

    let mut raw_subsidy: u64 = 50 * 10u64.pow(8);
    raw_subsidy >>= halvings;
    return raw_subsidy as f64 / 10u64.pow(8) as f64;
}

pub fn process_block(chunk: hyper::Chunk) -> String {
    let rblock: RPCBlock =
        match serde_json::from_slice(&chunk) {
            Ok(x) => x,
            Err(y) => {
                // TODO: distinctly suboptimal
                panic!("oh for fuck's sake {}", y);
            },
        };

    let subsidy = block_subsidy(rblock.height);

    let mut reward: f64 = 0.;
    for vout in &rblock.tx[0].vout {
        reward += vout.value;
    }

    let fees = reward - subsidy;

    let mut tx_count: u32 = 0;
    let mut tx_count_segwit: u32 = 0;
    for tx in &rblock.tx {
        tx_count += 1;
        if tx.hash != tx.txid {
            tx_count_segwit += 1;
        }
    }

    let pblockhash = match rblock.previousblockhash {
        Some(x) => x,
        None => "its_a_baby".to_string(),
    };

    let bblock = BlockinfoBlock {
        chainwork: rblock.chainwork,
        difficulty: rblock.difficulty,
        hash: rblock.hash,
        height: rblock.height,
        time: rblock.time,
        previousblockhash: pblockhash,
        size: rblock.size,
        weight: rblock.weight,
        subsidy: subsidy,
        fees: fees,
        tx_count: tx_count,
        tx_count_segwit: tx_count_segwit,
    };

    let serialized = match serde_json::to_string(&bblock) {
        Ok(x) => x,
        Err(y) => {
            eprintln!("couldn't serialize cos {}", y);
            String::from("MATE")
        },
    };
    log(&format!("did that as well!"));
    serialized
}
