//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate serde_json;
extern crate chrono;

use self::chrono::Utc;
use log::log;

#[derive(Serialize, Deserialize, Debug)]
struct RPCBTResp {
    result: RPCBlockTemplate,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCBlockTemplate {
    transactions: Vec<RPCBlockTemplateTx>,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCBlockTemplateTx {
    weight: u32,
    fee: f64,
    depends: Vec<serde_json::Value>,
}

#[derive(Serialize, Deserialize, Debug)]
struct Blocktemplate {
    time: u64,
    tx: Vec<(f64, u32, u32)>,
}

pub fn process_blocktemplate(chunk: hyper::Chunk) -> String {
    let rbtresp: RPCBTResp =
        match serde_json::from_slice(&chunk) {
            Ok(x) => x,
            Err(y) => {
                // TODO: distinctly suboptimal
                println!("{:?}", chunk);
                panic!("oh for fuck's sake {}", y);
            },
        };

    let time: u64 = Utc::now().timestamp() as u64;

    let mut tx: Vec<(f64, u32, u32)> = Vec::new();
    for ptx in &rbtresp.result.transactions {
        let vsize = (ptx.weight + 3) / 4;
        let feerate = ptx.fee as f64 / vsize as f64;
        let ndepends = ptx.depends.len() as u32;
        tx.push((feerate, vsize, ndepends));
    }

    let bt = Blocktemplate {
        time: time,
        tx: tx,
    };

    let serialized = match serde_json::to_string(&bt) {
        Ok(x) => x,
        Err(y) => {
            eprintln!("couldn't serialize cos {}", y);
            String::from("MATE")
        },
    };
    log(&format!("did that as well!"));
    serialized
}
