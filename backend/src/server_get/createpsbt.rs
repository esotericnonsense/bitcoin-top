//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
use hyper::{Body, Response};
use hyper::rt::{Future};
extern crate serde_json;

//use PgPool;
//use log::log;

use bitcoin_rpc;
use bitcoin_rpc::BitcoinRPCParameter::{self, Any};

#[derive(Serialize, Deserialize, Debug)]
struct RPCCRTResp {
    result: String,
}

type BoxFut = Box<dyn Future<Item=Response<Body>, Error=hyper::Error> + Send>;

// TODO: use dummy change addresses?

pub fn create_psbt(rpc_auth: &str, rpc_addr: &str, address: &str, amount: f64, feerate: f64) -> BoxFut {
    let a = Any(json!([]));
    let b = Any(json!({address: amount}));
    //serde_json::from_str(
    //    &format!("[{{\"{}\":{}}}]", address, amount)
    //).unwrap();
    let c = Any(json!(0));
    let d = Any(json!({"feeRate": feerate}));

    let proxied = bitcoin_rpc::get_resp_future_rpc(
        "walletcreatefundedpsbt".to_string(),
        Some(vec![a, b, c, d]),
        rpc_auth,
        rpc_addr,
    );

    return Box::new(proxied);
}

pub fn decode_psbt(rpc_auth: &str, rpc_addr: &str, psbt: &str) -> BoxFut {
    let proxied = bitcoin_rpc::get_resp_future_rpc(
        "decodepsbt".to_string(),
        Some(vec![BitcoinRPCParameter::Str(psbt.to_string())]),
        rpc_auth,
        rpc_addr,
    );

    return Box::new(proxied);
}

pub fn process_psbt(rpc_auth: &str, rpc_addr: &str, psbt: &str) -> BoxFut {
    let proxied = bitcoin_rpc::get_resp_future_rpc(
        "walletprocesspsbt".to_string(),
        Some(vec![BitcoinRPCParameter::Str(psbt.to_string())]),
        rpc_auth,
        rpc_addr,
    );

    return Box::new(proxied);
}

pub fn finalize_psbt(rpc_auth: &str, rpc_addr: &str, psbt: &str) -> BoxFut {
    let proxied = bitcoin_rpc::get_resp_future_rpc(
        "finalizepsbt".to_string(),
        Some(vec![BitcoinRPCParameter::Str(psbt.to_string())]),
        rpc_auth,
        rpc_addr,
    );

    return Box::new(proxied);
}

pub fn sendrawtransaction(rpc_auth: &str, rpc_addr: &str, hex: &str) -> BoxFut {
    let proxied = bitcoin_rpc::get_resp_future_rpc(
        "sendrawtransaction".to_string(),
        Some(vec![BitcoinRPCParameter::Str(hex.to_string())]),
        rpc_auth,
        rpc_addr,
    );

    return Box::new(proxied);
}
