//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate serde_json;
extern crate chrono;

use self::chrono::{Utc};

use std::collections::{HashSet, HashMap, BTreeMap};

#[derive(Serialize, Deserialize, Debug)]
struct RestMempoolTx {
    wtxid: Option<String>,
    fee: f64,
    size: u32,
}

type RestMempoolContents = HashMap<String, RestMempoolTx>;

#[derive(Serialize, Deserialize, Debug)]
struct MempoolBins {
    time: f64,
    size: u32,
    size_segwit: u32,
    bytes: u32,
    fees: f64,
    bins: Vec<(u64, u64, u32, u32, f64, u32, u32, f64)>,
}

fn bin_it(n: f64) -> u64 {
    // This function is kind of odd, but it's legacy now. :(
    let mut x = n.round() + 0.00001;

    if x < 5.0 {
        return x.ceil() as u64
    } else if x < 30.0 {
        x /= 5.0;
        return x.ceil() as u64 * 5
    } else if x < 60.0 {
        x /= 30.0;
        return x.ceil() as u64 * 30
    } else if x < 300.0 {
        x /= 50.0;
        return x.ceil() as u64 * 50
    } else if x < 500.0 {
        x /= 100.0;
        return x.ceil() as u64 * 100
    } else if x < 1000.0 {
        x /= 500.0;
        return x.ceil() as u64 * 500
    }

    21*10u64.pow(14) // wowee that's some bitcoins
}

pub fn process_mempoolbins(chunk: hyper::Chunk) -> (i64, String) {
    let rmcontents: RestMempoolContents =
        match serde_json::from_slice(&chunk) {
            Ok(x) => x,
            Err(y) => {
                // TODO: distinctly suboptimal
                panic!("oh for fuck's sake {}", y);
            },
        };

    // TODO TODO TODO
    // This seems to not be equivalent to the Python version.
    // Must revisit.

    let now = Utc::now();
    let time = (now.timestamp() * 1000) + (now.timestamp_subsec_millis() as i64);

    let mut binvec = Vec::new();
    let mut hs = HashSet::new();
    for x in 1..1001 {
        let binned = bin_it(x as f64);
        if !hs.contains(&binned) {
            binvec.push(binned);
            hs.insert(binned);
        }
    }
    binvec.sort();

    let mut bins = BTreeMap::new();
    for bin in binvec {
        bins.insert(bin, Vec::new());
    }

    let mut size_segwit: u32 = 0;
    for (txid, tx) in &rmcontents {
        match &tx.wtxid {
            Some(wtxid) => {
                if wtxid != txid { size_segwit += 1 };
            },
            None => {
                panic!("is bitcoind very old what is this")
            },
        }

        let fee_in_sat = tx.fee * (10u64.pow(8) as f64);
        let sat_b = fee_in_sat / (tx.size as f64);
        let bin = bin_it(sat_b);

        bins.get_mut(&bin).unwrap().push((tx.size, fee_in_sat));
    }

    let mut binned = Vec::new();
    //Vec<(u64, u64, u32, u32, f64, u32, u32, f64)>,
    let mut prevbin: u64 = 1;
    let mut cum_size: u32 = 0;
    let mut cum_bytes: u32 = 0;
    let mut cum_fees: f64 = 0.;
    for (bin, txv) in bins {
        let total_size = txv.len() as u32;
        cum_size += total_size;

        let total_bytes = txv.iter().map(|x| x.0).sum();
        cum_bytes += total_bytes;

        let total_fees = txv.iter().map(|x| x.1).sum();
        cum_fees += total_fees;

        binned.push(
            (prevbin, bin, total_size, total_bytes, total_fees, cum_size, cum_bytes, cum_fees)
        );

        prevbin = bin;
    }

    binned.reverse();
    let mpb = MempoolBins {
        time: (time as f64) / 1000.0,
        size: cum_size,
        size_segwit: size_segwit,
        bytes: cum_bytes,
        fees: cum_fees,
        bins: binned,
    };

    let serialized = match serde_json::to_string(&mpb) {
        Ok(x) => x,
        Err(y) => {
            eprintln!("couldn't serialize cos {}", y);
            String::from("MATE")
        },
    };
    (time, serialized)
}
