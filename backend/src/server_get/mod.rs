//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate chrono;
use self::chrono::Utc;
extern crate futures;
use futures::future::poll_fn;
extern crate hyper;
use hyper::{Body, Response};
use hyper::rt::{Future, Stream};
use hyper::header::{HeaderName, HeaderValue};
extern crate serde_json;
extern crate tokio_threadpool;
use self::tokio_threadpool::blocking;

use PgPool;
use log::log;

type BoxFut = Box<dyn Future<Item=Response<Body>, Error=hyper::Error> + Send>;

mod blockinfo;
mod blocktemplate;
pub mod mempoolbins;
pub mod nettotals;
mod peerinfo;
mod whitelist;
mod createpsbt;

use server_get::whitelist::WHITELIST;

use bitcoin_rpc;
use bitcoin_rpc::BitcoinRPCParameter;

#[derive(Serialize, Deserialize, Debug)]
struct RPCCreatePSBTResp {
    result: RPCCreatePSBT,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCCreatePSBT {
    psbt: String,
    fee: f64,
    changepos: u32,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCResp<T> {
    result: T,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCPSBT {
    psbt: String,
    complete: bool,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCRawTx {
    hex: String,
    complete: bool,
}

fn add_headers(response: &mut Response<Body>, cache_secs: usize) {
    let hn = HeaderName::from_lowercase(b"cache-control").unwrap();
    let hv = HeaderValue::from_str(
        &format!("max-age={}", cache_secs)
    ).unwrap();

    // TODO: what does upstream send?
    //  can we simply proxy it?
    response.headers_mut().insert(hn, hv);
    let hhn = HeaderName::from_lowercase(b"content-type").unwrap();
    let hhv = HeaderValue::from_str(
        &format!("application/json")
    ).unwrap();
    response.headers_mut().insert(hhn, hhv);
}

pub fn server_get(x: &str, pool: &PgPool, rpc_auth: &str, rpc_addr: &str) -> BoxFut {
    let mut response = Response::new(Body::empty());

    if x.ends_with(".json") {
        log(&format!("JSON: {}", x));

        let proxied = bitcoin_rpc::get_resp_future_rest(
                x.to_string(),
                rpc_addr,
            )
            .and_then(move |res| {
                res.into_body()
                .concat2()
                .map(move |chunk| {
                    add_headers(&mut response, 5);
                    *response.body_mut() = Body::from(chunk);
                    response
                })
            });

        return Box::new(proxied);
    }

    let raw_args: Vec<&str> = x.split("/").collect();
    if raw_args.len() < 2 || raw_args[1].len() == 0 { // e.g. "/"
        log(&format!("INVALID: {}", x));

        *response.body_mut() = Body::from(
            "{\"result\": null, \"error\": \"go away\", \"id\": \"na\"}\n"
        );
        return Box::new(futures::future::ok(response));
    }

    let method: &str = raw_args[1];

    // TODO: le hacks.
    if method == "blockinfo" {
        eprintln!("ey lads its a blockinfo innit");
        if raw_args.len() < 3 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"needs more arguments lad\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }
        if raw_args[2].len() != 64 {
            // it's an error!
            *response.body_mut() = Body::from(
                "thats not a block hash mate"
            );
            return Box::new(futures::future::ok(response));
        }

        let blockhash = raw_args[2];
        log(&format!("getting a blockinfo!"));
        let proxied = bitcoin_rpc::get_resp_future_rest(
                format!("/rest/block/{}.json", blockhash),
                rpc_addr,
            )
            .and_then(move |res| {
                res.into_body()
                .concat2()
                .map(move |chunk| {
                    log(&format!("man that took a while!"));
                    add_headers(&mut response, 600);
                    let block = blockinfo::process_block(chunk);
                    *response.body_mut() = Body::from(block);
                    response
                })
            });

        return Box::new(proxied);
    }

    // TODO: le hacks.
    if method == "peerinfo" {
        eprintln!("ey lads its a peerinfo innit");
        if raw_args.len() > 2 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"too many arguments lad\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }

        let proxied = bitcoin_rpc::get_resp_future_rpc(
                "getpeerinfo".to_string(),
                None,
                rpc_auth,
                rpc_addr,
            )
            .and_then(move |res| {
                res.into_body()
                .concat2()
                .map(move |chunk| {
                    add_headers(&mut response, 5);
                    let block = peerinfo::process_peerinfo(chunk);
                    *response.body_mut() = Body::from(block);
                    response
                })
            });

        return Box::new(proxied);
    }

    // TODO: le hacks.
    if method == "blocktemplate" {
        eprintln!("ey lads its a blocktemplate innit");
        if raw_args.len() > 2 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"too many arguments lad\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }

        let proxied = bitcoin_rpc::get_resp_future_rpc(
                "getblocktemplate".to_string(),
                None,
                rpc_auth,
                rpc_addr,
            )
            .and_then(move |res| {
                res.into_body()
                .concat2()
                .map(move |chunk| {
                    add_headers(&mut response, 5);
                    let block = blocktemplate::process_blocktemplate(chunk);
                    *response.body_mut() = Body::from(block);
                    response
                })
            });

        return Box::new(proxied);
    }

    if method == "mempoolbins" {
        eprintln!("ey lads its a mempoolbins innit");
        if raw_args.len() > 2 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"too many arguments lad\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }

        log(&format!("getting a mempoolbins!"));
        let proxied = bitcoin_rpc::get_resp_future_rest(
                "/rest/mempool/contents.json".to_string(),
                rpc_addr,
            )
            .and_then(move |res| {
                res.into_body()
                .concat2()
                .map(move |chunk| {
                    log(&format!("man that took a while!"));
                    add_headers(&mut response, 5);
                    let (_, block) = mempoolbins::process_mempoolbins(chunk);
                    *response.body_mut() = Body::from(block);
                    response
                })
            });

        return Box::new(proxied);
    }

    if method == "nettotals" {
        eprintln!("ey lads its a nettotals innit");
        if raw_args.len() > 2 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"too many arguments lad\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }

        let proxied = bitcoin_rpc::get_resp_future_rpc(
                "getnettotals".to_string(),
                None,
                rpc_auth,
                rpc_addr,
            )
            .and_then(move |res| {
                res.into_body()
                .concat2()
                .map(move |chunk| {
                    add_headers(&mut response, 5);
                    let (_, block) = nettotals::process_nettotals(chunk);
                    *response.body_mut() = Body::from(block);
                    response
                })
            });

        return Box::new(proxied);
    }

    if method == "mempoolbinsrange" {
        eprintln!("ey lads its a mempoolbinsrange innit");
        if raw_args.len() < 3 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"not enough arguments lad\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }
        let minutes = match raw_args[2].parse::<i64>() {
            Ok(x) => x,
            Err(_) => {
                *response.body_mut() = Body::from(
                    "stop it"
                );
                return Box::new(futures::future::ok(response));
            },
        };

        let now = Utc::now().timestamp() as i64;
        let end_utc_ms = now * 1000;
        let interval_ms = minutes * 60 * 1000;
        let start_utc_ms = end_utc_ms - interval_ms;
        let step_ms = interval_ms / 120;

        let conn = pool.get().unwrap();
        let test = poll_fn(move || {
            blocking(|| {
                let sq = "
                WITH y AS (
                    WITH t AS (
                        SELECT * FROM generate_series($1::int8, $2::int8, $3::int8) AS utc_ms
                    )
                    SELECT (SELECT MIN(utc_ms) FROM mempoolbins WHERE utc_ms > t.utc_ms)
                    FROM t WHERE t.utc_ms IS NOT NULL
                )
                SELECT * FROM mempoolbins WHERE utc_ms IN (SELECT * FROM y);
                ";
                // TODO how do I split this out?
                let q = &conn.query(sq, &[&start_utc_ms, &end_utc_ms, &step_ms]).unwrap();
                let mut items = Vec::new();
                for row in q {
                    let (_, json): (i64, String) = (row.get(0), row.get(1));
                    items.push(json);
                }
                items
            }).map_err(|_| panic!("omg did a bad!!"))
        })
        .and_then(move |items| {
            let mut output = "{\"list\":[".to_string();
            let temp = items.join(",");
            output.push_str(&temp);
            output.push_str("]}");

            let cache_secs = ((interval_ms as u64) / 2000) as usize;
            let cache_secs = cache_secs / 120;
            add_headers(&mut response, cache_secs);
            *response.body_mut() = Body::from(output);
            futures::future::ok(response)
        });

        return Box::new(test);
    }

    if method == "nettotalsrange" {
        eprintln!("ey lads its a nettotalsrange innit");
        if raw_args.len() < 3 {
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"not enough args\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }
        let minutes = match raw_args[2].parse::<i64>() {
            Ok(x) => x,
            Err(_) => {
                *response.body_mut() = Body::from(
                    "{\"result\": null, \"error\": \"not an integer\", \"id\": \"na\"}\n"
                );
                return Box::new(futures::future::ok(response));
            },
        };

        let now = Utc::now().timestamp() as i64;
        let end_utc_ms = now * 1000;
        let interval_ms = minutes * 60 * 1000;
        let start_utc_ms = end_utc_ms - interval_ms;
        let step_ms = interval_ms / 120;

        let conn = pool.get().unwrap();
        let test = poll_fn(move || {
            blocking(|| {
                let sq = "
                WITH y AS (
                    WITH t AS (
                        SELECT * FROM generate_series($1::int8, $2::int8, $3::int8) AS utc_ms
                    )
                    SELECT (SELECT MIN(utc_ms) FROM nettotals WHERE utc_ms > t.utc_ms)
                    FROM t WHERE t.utc_ms IS NOT NULL
                )
                SELECT * FROM nettotals WHERE utc_ms IN (SELECT * FROM y);
                ";
                // TODO how do I split this out?
                let q = &conn.query(sq, &[&start_utc_ms, &end_utc_ms, &step_ms]).unwrap();
                let mut items = Vec::new();
                for row in q {
                    let (_, json): (i64, String) = (row.get(0), row.get(1));
                    items.push(json);
                }
                items
            }).map_err(|_| panic!("omg did a bad!!"))
        })
        .and_then(move |items| {
            let mut output = "{\"list\":[".to_string();
            let temp = items.join(",");
            output.push_str(&temp);
            output.push_str("]}");

            let cache_secs = ((interval_ms as u64) / 2000) as usize;
            let cache_secs = cache_secs / 120;
            add_headers(&mut response, cache_secs);
            *response.body_mut() = Body::from(output);
            futures::future::ok(response)
        });

        return Box::new(test);
    }

    if method == "createpsbt" {
        if raw_args.len() != 5 {
            eprintln!("{:#?}", raw_args);
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"createpsbt/address/amount/feerate\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }

        let address: &str = raw_args[2];
        let amount = match raw_args[3].parse::<f64>() {
            Ok(x) => x,
            Err(_) => {
                *response.body_mut() = Body::from(
                    "{\"result\": null, \"error\": \"amount is not a number\", \"id\": \"na\"}\n"
                );
                return Box::new(futures::future::ok(response));
            },
        };
        let feerate = match raw_args[4].parse::<f64>() {
            Ok(x) => x,
            Err(_) => {
                *response.body_mut() = Body::from(
                    "{\"result\": null, \"error\": \"feerate is not a number\", \"id\": \"na\"}\n"
                );
                return Box::new(futures::future::ok(response));
            },
        };

        let rpc_auth = rpc_auth.to_string();
        let rpc_addr = rpc_addr.to_string();
        add_headers(&mut response, 1);
        let proxied = createpsbt::create_psbt(
            &rpc_auth, &rpc_addr, address, amount, feerate
        )
        .and_then(move |res| {
            res
            .into_body()
            .concat2()
            .and_then(move |c| {
                let rr: RPCCreatePSBTResp =
                    match serde_json::from_slice(&c) {
                        Ok(x) => x,
                        Err(y) => {
                            // TODO: distinctly suboptimal
                            panic!("oh for fuck's sake {}", y);
                        },
                    };

                let rrr = rr.result;

                createpsbt::decode_psbt(
                    &rpc_auth, &rpc_addr, &rrr.psbt
                )
                .and_then(move |res| {
                    res
                    .into_body()
                    .concat2()
                    .map(move |c| {
                        let j: serde_json::Value =
                            match serde_json::from_slice(&c) {
                                Ok(x) => x,
                                Err(y) => {
                                    // TODO: distinctly suboptimal
                                    panic!("oh for fuck's sake {}", y);
                                },
                            };

                        // seriously
                        let jr = j.as_object().unwrap()
                            .get("result").unwrap().as_object().unwrap();

                        let mut jr = jr.clone();

                        jr.insert(
                            "psbt".to_string(),
                            json!(rrr.psbt)
                        );
                        jr.insert(
                            "fee".to_string(),
                            json!(rrr.fee)
                        );
                        jr.insert(
                            "changepos".to_string(),
                            json!(rrr.changepos)
                        );

                        let ser = serde_json::to_string(&jr).unwrap();

                        *response.body_mut() = Body::from(ser);
                        response
                    })
                })
            })
        });


        return Box::new(proxied);
    }

    if method == "submitpsbt" {
        if raw_args.len() != 3 {
            eprintln!("{:#?}", raw_args);
            *response.body_mut() = Body::from(
                "{\"result\": null, \"error\": \"submitpsbt/psbt\", \"id\": \"na\"}\n"
            );
            return Box::new(futures::future::ok(response));
        }

        let rpsbt: &str = raw_args[2];
        // "decode" from urlsafe base64 to standard
        let mut s = rpsbt.replace("-","+").replace("_","/").to_string();
        let padding = (4 - (s.len() % 4)) % 4;
        for _ in 0..padding {
            s.push('=');
        }

        let rpc_auth = rpc_auth.to_string();
        let rpc_addr = rpc_addr.to_string();
        add_headers(&mut response, 1);
        let proxied = createpsbt::process_psbt(
            &rpc_auth, &rpc_addr, &s
        )
        .and_then(move |res| {
            res
            .into_body()
            .concat2()
            .and_then(move |c| {
                let r: RPCResp<RPCPSBT> =
                    match serde_json::from_slice(&c) {
                        Ok(x) => x,
                        Err(y) => {
                            // TODO: distinctly suboptimal
                            eprintln!("{:#?}", c);
                            panic!("oh for fuck's sake {}", y);
                        },
                    };

                createpsbt::finalize_psbt(
                    &rpc_auth, &rpc_addr, &r.result.psbt
                )
                .and_then(move |res| {
                    res
                    .into_body()
                    .concat2()
                    .and_then(move |c| {
                        let r: RPCResp<RPCRawTx> =
                            match serde_json::from_slice(&c) {
                                Ok(x) => x,
                                Err(y) => {
                                    // TODO: distinctly suboptimal
                                    panic!("oh for fuck's sake {}", y);
                                },
                            };

                        createpsbt::sendrawtransaction(
                            &rpc_auth, &rpc_addr, &r.result.hex
                        )
                        .and_then(move |res| {
                            res
                            .into_body()
                            .concat2()
                            .map(move |c| {
                                *response.body_mut() = Body::from(c);
                                response
                            })
                        })
                    })
                })
            })
        });

        return Box::new(proxied);
    }

    // done with weird custom methods now

    let (cache_secs, argument_cap, _) = match WHITELIST.get(method) {
        Some(x) => x,
        None => {
            log(&format!("FORBIDDEN: {}", x));
            *response.body_mut() = Body::from("{\"result\": null, \"error\": \"ERROR: denied by backend\", \"id\": \"na\"}\n");
            return Box::new(futures::future::ok(response));
        },
    };

    let params: Option<Vec<BitcoinRPCParameter>>;
    if raw_args.len() < 3 {
        params = None;
    } else if raw_args.len() > (2 + argument_cap) {
        log(&format!("TOO_MANY_PARAMS: {}", x));

        *response.body_mut() = Body::from("{\"result\": null, \"error\": \"ERROR: denied by backend\", \"id\": \"na\"}\n");
        return Box::new(futures::future::ok(response));
    } else {
        let mut tparams: Vec<BitcoinRPCParameter> = Vec::new();
        for raw_param in &raw_args[2..] {
            let tparam: BitcoinRPCParameter = match raw_param.parse::<bool>() {
                Ok(x) => BitcoinRPCParameter::Bool(x),
                Err(_) => {
                    match raw_param.parse::<i64>() {
                        Ok(x) => BitcoinRPCParameter::Int(x),
                        Err(_) => BitcoinRPCParameter::Str(raw_param.to_string()),
                    }
                }
            };
            tparams.push(tparam);
        }
        params = Some(tparams);
    }

    log(&format!("proxied: {}", x));
    let proxied = bitcoin_rpc::get_resp_future_rpc(
            method.to_string(),
            params,
            rpc_auth,
            rpc_addr,
        )
        .and_then(move |res| {
            res.into_body()
            .concat2()
            .map(move |chunk| {
                *response.body_mut() = Body::from(chunk);
                add_headers(&mut response, *cache_secs);
                response
            })
        });

    Box::new(proxied)
}
