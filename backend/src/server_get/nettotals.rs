//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate serde_json;
extern crate chrono;

use self::chrono::Utc;

#[derive(Serialize, Deserialize, Debug)]
struct RPCNTResp {
    result: RPCNettotals,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCNettotals {
    totalbytesrecv: u64,
    totalbytessent: u64,
}

#[derive(Serialize, Deserialize, Debug)]
struct Nettotals {
    time: f64,
    totalbytesrecv: u64,
    totalbytessent: u64,
}

pub fn process_nettotals(chunk: hyper::Chunk) -> (i64, String) {
    let rntresp: RPCNTResp =
        match serde_json::from_slice(&chunk) {
            Ok(x) => x,
            Err(y) => {
                // TODO: distinctly suboptimal
                println!("{:?}", chunk);
                panic!("oh for fuck's sake {}", y);
            },
        };

    let now = Utc::now();
    let time = (now.timestamp() * 1000) + (now.timestamp_subsec_millis() as i64);

    let nt = Nettotals {
        time: (time as f64) / 1000.0,
        totalbytesrecv: rntresp.result.totalbytesrecv,
        totalbytessent: rntresp.result.totalbytessent,
    };

    let serialized = match serde_json::to_string(&nt) {
        Ok(x) => x,
        Err(y) => {
            eprintln!("couldn't serialize cos {}", y);
            String::from("MATE")
        },
    };
    (time, serialized)
}
