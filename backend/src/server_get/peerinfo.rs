//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

extern crate hyper;
extern crate serde_json;

use log::log;

#[derive(Serialize, Deserialize, Debug)]
struct RPCPIResp {
    result: Vec<RPCPeerinfo>,
}

#[derive(Serialize, Deserialize, Debug)]
struct RPCPeerinfo {
    addr: String,
    bytesrecv: u64,
    bytessent: u64,
    conntime: u64,
    inbound: bool,
    synced_blocks: i32,
    subver: String,
}

pub fn process_peerinfo(chunk: hyper::Chunk) -> String {
    let rpiresp: RPCPIResp =
        match serde_json::from_slice(&chunk) {
            Ok(x) => x,
            Err(y) => {
                // TODO: distinctly suboptimal
                println!("{:?}", chunk);
                panic!("oh for fuck's sake {}", y);
            },
        };

    let serialized = match serde_json::to_string(&rpiresp.result) {
        Ok(x) => x,
        Err(y) => {
            eprintln!("couldn't serialize cos {}", y);
            String::from("MATE")
        },
    };
    log(&format!("did that as well!"));
    serialized
}
