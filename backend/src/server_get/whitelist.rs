//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

use std::collections::HashMap;
lazy_static! {
    pub static ref WHITELIST: HashMap<&'static str, (usize, usize, usize)> =
    [
//  COMMAND               CACHE_S  ARGS  AUTH
    ("logging",                (1,    2,    0)), // "include", "exclude"
    ("getblockchaininfo",      (1,    0,    0)),
    ("getchaintxstats",        (1,    2,    0)), // "nblocks", "blockhash"
    ("getblockstats",          (1,    2,    0)), // "hash_or_height", "stats"
    ("getbestblockhash",       (1,    0,    0)),
    ("getblockcount",          (1,    0,    0)),
    ("getblock",               (1,    2,    0)), // "blockhash","verbosity|verbose"
    ("getblockhash",           (1,    1,    0)), // "height"
    ("getblockheader",         (1,    2,    0)), // "blockhash","verbose"
    ("getchaintips",           (1,    0,    0)),
    ("getdifficulty",          (1,    0,    0)),
    ("getmempoolancestors",    (1,    2,    0)), // "txid","verbose"
    ("getmempooldescendants",  (1,    2,    0)), // "txid","verbose"
    ("getmempoolentry",        (1,    1,    0)), // "txid"
    ("getmempoolinfo",         (1,    0,    0)),
    ("getrawmempool",          (1,    1,    0)), // "verbose"
    ("gettxout",               (1,    3,    0)), // "txid","n","include_mempool"
    ("gettxoutsetinfo",        (1,    0,    0)),
    ("getconnectioncount",     (1,    0,    0)),
    ("ping",                   (1,    0,    0)),
    ("getpeerinfo",            (1,    0,    0)),
    ("getnettotals",           (1,    0,    0)),
    ("getnetworkinfo",         (1,    0,    0)),
    ("getnetworkhashps",       (1,    2,    0)), // "nblocks","height"
    ("getmininginfo",          (1,    0,    0)),
    ("getblocktemplate",       (1,    1,    0)), // "template_request"
    ("estimatesmartfee",       (1,    2,    0)), // "conf_target", "estimate_mode"
    ("estimaterawfee",         (1,    2,    0)), // "conf_target", "threshold"
    ("uptime",                 (1,    0,    0)),
    ("help",                   (1,    1,    0)), // "command"
    ("getwalletinfo",          (1,    0,    1)),
    ("getaddressesbylabel",    (1,    1,    1)), // "label"
    ("getnewaddress",          (1,    2,    1)), // "label", "address_type"
    ("listreceivedbyaddress",  (1,    4,    1)), // "minconf", "include_empty", "include_watchonly", "address_filter"
    ("listtransactions",       (1,    4,    1)), // "dummy", "count", "skip", "include_watchonly",
    ]
     .iter().cloned().collect();
}
