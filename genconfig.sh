#!/bin/bash

#
# Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
# Electron Relocation Limited
#
# This file is part of bitcoin-top.  bitcoin-top is free software: you can
# redistribute it and/or modify it under the terms of the GNU Affero General
# Public License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
#

### begin user configurable section ###

# webserver listen port
BM_PORT=3000

# postgresql database host - unset to spawn a container
BM_DBHOST=
# postgresql user / role - unset to use default of "bm"
BM_DBROLE=
# postgresql database name - unset to use default of "bm"
BM_DBNAME=
# postgresql password - unset to use randomly generated
BM_DBPASS=

# bitcoind rpc host - unset to spawn a container
BM_RPCADDR=
# bitcoind rpc username - unset to use randomly generated
BM_RPCUSER=
# bitcoind rpc password - unset to use randomly generated
BM_RPCPASS=
# additional arguments for bitcoind; e.g. -regtest
BCDARGS=

### end user configurable section ###

function gen_password {
    cat /dev/urandom | tr -dc "a-zA-Z0-9"| fold -w 24 | head -n 1
}

if [[ -z "$BM_DBNAME" ]]; then
    # TODO assert DBHOST set if nonzero
    BM_DBNAME=bm
fi
if [[ -z "$BM_DBROLE" ]]; then
    # TODO assert DBHOST set if nonzero
    BM_DBROLE=bm
fi
if [[ -z "$BM_DBPASS" ]]; then
    # TODO assert DBHOST set if nonzero
    BM_DBPASS=$(gen_password)
fi

if [[ -z "$BM_RPCUSER" ]]; then
    # TODO assert RPCADDR set if nonzero
    BM_RPCUSER=$(gen_password)
fi
if [[ -z "$BM_RPCPASS" ]]; then
    # TODO assert RPCADDR set if nonzero
    BM_RPCPASS=$(gen_password)
fi


TEMPLATES=deploy/docker-compose.template.yml
if [[ -z "$BM_DBHOST" ]]; then
    BM_DBHOST=postgres
    TEMPLATES="${TEMPLATES} deploy/postgres.template.yml"
fi
if [[ -z "$BM_RPCADDR" ]]; then
    BM_RPCADDR=bitcoind:38332
    TEMPLATES="${TEMPLATES} deploy/bitcoind.template.yml"
fi

echo "Using templates ${TEMPLATES}..."

function get_short_date {
    date --utc --iso-8601=seconds | cut -d"+" -f1 | sed "s/://g" | sed "s/-//g" | sed "s/T/-/g"
}
DCBACKUP=docker-compose.yml.$(get_short_date).bak
>&2 echo "Creating $DCBACKUP..."
mv -i docker-compose.yml $DCBACKUP

cat $TEMPLATES \
    | grep -v "^#" \
    | sed "s/___BMDBNAME___/$BM_DBNAME/g" \
    | sed "s/___BMDBROLE___/$BM_DBROLE/g" \
    | sed "s/___BMDBHOST___/$BM_DBHOST/g" \
    | sed "s/___BMDBPASS___/$BM_DBPASS/g" \
    | sed "s/___BMRPCADDR___/$BM_RPCADDR/g" \
    | sed "s/___BMRPCUSER___/$BM_RPCUSER/g" \
    | sed "s/___BMRPCPASS___/$BM_RPCPASS/g" \
    | sed "s/___BMPORT___/$BM_PORT/g" \
    | sed "s/___BCDARGS___/$BCDARGS/g" > docker-compose.yml

>&2 echo "Configuration written to docker-compose.yml"
