# /etc/nginx/sites-available/default

#
# Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
# Electron Relocation Limited
#
# This file is part of bitcoin-top.  bitcoin-top is free software: you can
# redistribute it and/or modify it under the terms of the GNU Affero General
# Public License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
#

server {
    listen [::]:80 default_server;
    listen 80 default_server;

    root /usr/share/nginx/html;
    index index.html;

    # This should be logged further upstream
    access_log off;

    # None of the data provided by this should be security critical.
    gzip on;
    gzip_proxied any;
    gzip_http_version 1.0;
    gzip_types text/plain text/json text/javascript text/xml application/json text/css application/javascript;

    location / {
        # First attempt to serve request as file, then
        # as directory, then fall back to displaying a 404.
        try_files $uri $uri/ =404;
    }

    location /api/ {
        proxy_pass http://backend/;
    }
}
