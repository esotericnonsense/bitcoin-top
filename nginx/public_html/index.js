//
// Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
// Electron Relocation Limited
//
// This file is part of bitcoin-top.  bitcoin-top is free software: you can
// redistribute it and/or modify it under the terms of the GNU Affero General
// Public License as published by the Free Software Foundation, either version 3
// of the License, or (at your option) any later version.
//
// bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
// details.
//
// You should have received a copy of the GNU Affero General Public License
// along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
//

const THIS_URL = `${window.location.protocol}//${window.location.host}`;
const API_URL = `${window.location.protocol}//${window.location.host}/api`;
const ALL_BITCOINS = 21*(10**14);
const INITIAL_RANGE = 480;

const TABS = [
    ["dashboard", "Dashboard"],
    ["wallet-receive", "Wallet (receive)"],
    ["wallet-send", "Wallet (send)"],
    ["block-template", "Block template"],
    ["mempool-table", "Mempool table"],
    ["blocks", "Blocks"],
    ["peers", "Peers"],
    ["network-chart", "Network chart"],
    ["debug-console", "Debug console"],
    ["about", "About"],
];

const RANGES = [
    [30, "30m"],
    [60, "1h"],
    [120, "2h"],
    [240, "4h"],
    [480, "8h"],
    [1440, "24h"],
    [2880, "2d"],
    [5760, "4d"],
    [10080, "7d"],
    [20160, "14d"],
    [40320, "28d"],
    [80640, "2mo"],
    [161280, "4mo"],
    [322560, "8mo"],
    [483840, "1y"],
];
const DATAS = [
    [5, "tx"],
    [6, "size / MiB"],
    [7, "amt / mBTC"],
];

var formatBytes = function(a,b){if(0==a)return"zero";var c=1024,d=b||2,e=["B","KiB","MiB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]};

var getDateEpoch = function(d) {
    return (d.getTime() / 1000);
}
var getCurrentTime = function() {
    return getDateEpoch(new Date());
};
var getCurrentTimeUTC = function() {
    let now = new Date();
    return Math.floor((now.getTime() + now.getTimezoneOffset() * 60000)/1000);
};

var rawAsyncRequest = function(request, callback) {
    asyncRequest(request, callback, true);
}

var asyncRequest = function(request, callback, raw=false) {
    var xhr = new XMLHttpRequest();
    let url = raw ? request : `${API_URL}/${request}`;
    xhr.open("GET", url, true);
    xhr.onload = function(e) {
        if (xhr.readyState === 4) {
            if (xhr.status === 200) {
                if (raw) {
                    callback(request, xhr.responseText);
                } else {
                    let j = JSON.parse(xhr.responseText);
                    callback(request, j);
                }
            } else {
                // console.error(xhr.statusText);
            }
        }
    };
    xhr.onerror = function(e) {
        // console.error(xhr.statusText);
    };
    xhr.send(null);
}

onload = function() {
    el_out = document.getElementById("output");

    let aspect_ratio = window.innerWidth/window.innerHeight;
    let mobile = aspect_ratio < 1.33; // smaller than 4:3

    var app = new Vue({
        el: '#app',
        data: {
            connected: false,
            now: getCurrentTime(),
            lastpong: new Date(0),
            last_updated: new Date(0),
            nav_hidden: mobile,
            uptime: null,
            chaininfo: null,
            mininginfo: null,
            walletinfo: null,
            mempoolbins: null,
            nettotals: null,
            blocktemplate: null,
            unused_address: null,
            unused_address_qr: null,
            wallet_send_address: null,
            wallet_send_amount: 0.05,
            wallet_send_feerate: 0.00001,
            wallet_createpsbt: null,
            transactions: null,
            git_status: null,
            authors_md: null,
            bt_logscale: true,
            debug_input: "",
            debug_output: "",
            peers: null,
            data: 7,
            datas: DATAS,
            range: 0,
            ranges: RANGES,
            tab: "dashboard",
            tabs: TABS,
            blocks: {},
            staging: window.location.host.includes("test"),
        },
        filters: {
            pretty: function(json) {
                return JSON.stringify(json, null, 2);
            },
            timeInterval: function(timeinterval) {
                if (timeinterval === null) {
                    return "unknown";
                }

                let y = Math.floor(timeinterval / 31536000);
                let d = Math.floor((timeinterval / 86400) % 365);
                let h = Math.floor((timeinterval / 3600) % 24);
                let m = Math.floor((timeinterval / 60) % 60);
                let s = Math.floor(timeinterval % 60);
                if (y) {
                    return `${y}y ${d}d`;
                } else if (d) {
                    return `${d}d ${h}h`;
                } else if (h) {
                    return `${h}h ${m}m`;
                } else if (m) {
                    return `${m}m ${s}s`;
                } else {
                    return `${s}s`;
                }
            },
            blockIntervalBar: function(seconds) {
                if (seconds < 0) {
                    return "";
                }
                let minutes = Math.floor(seconds / 60);
                return "*".repeat(minutes);
            },
        },
        computed: {
            truncatedTransactions: function() {
                return app.transactions.slice(0,3);
            },
            sortedBlocks: function() {
                let l = [];
                for (let hash of Object.keys(this.blocks)) {
                    l.push(this.blocks[hash]);
                };
                l.sort(function(a, b) {
                    let x = a.height; let y = b.height;
                    return ((x > y) ? -1 : ((x < y) ? 1 : 0));
                });
                return l;
            },
        },
        methods: {
            toggleScanner: function() {
                if (SCANNER_ENABLED) {
                    SCANNER.stop();
                    SCANNER_ENABLED = false;
                    return;
                }

                Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                    SCANNER.start(cameras[0]);
                    SCANNER_ENABLED = true;
                } else {
                    console.error('No cameras found.');
                }
                }).catch(function (e) {
                    console.error(e);
                });
            },
            submitSubmitPSBT: function() {
                if (app.wallet_createpsbt === null) { return; }
                let psbt = app.wallet_createpsbt.psbt;
                if (psbt === null) { return; }
                let tosend = psbt
                    .replace(/\+/g, "-")
                    .replace(/\//g, "_")
                    .replace(/=/g, "");

                let l = [
                    "submitpsbt",
                    tosend,
                ];
                let request = l.join("/");

                asyncRequest(request, dealWithSubmitPSBT);
            },
            submitCreatePSBT: function() {
                let l = [
                    "createpsbt",
                    app.wallet_send_address,
                    app.wallet_send_amount,
                    app.wallet_send_feerate,
                ];
                for (let x of l) {
                    if (x === null) { console.log(l); return; }
                }
                let request = l.join("/");
                asyncRequest(request, dealWithCreatePSBT);
            },
            submitDebugInput: function() {
                let request = app.debug_input.split(" ").join("/");
                asyncRequest(request, dealWithDebugOutput);
                app.debug_input = "";
            },
            toggleNav: function() {
                app.nav_hidden = !app.nav_hidden;
            },
            getBlockIfRequired: function(hash) {
                if (hash in app.blocks) {
                    return;
                }

                asyncRequest(`blockinfo/${hash}`, processAsyncResponse);
            },
            setRange: function(range) {
                if (range === app.range) {
                    return;
                }

                COLUMNS = null;

                // when the response comes in the graph will reset.
                clearBinInterval();
                setBinInterval(range);
            },
            setData: function(data) {
                if (data === app.data) {
                    return;
                }

                app.data = data;

                // TODO: this is pretty hacky.
                COLUMNS = null;

                // when the response comes in the graph will reset.
                clearBinInterval();
                setBinInterval(app.range);
            },
            setTab: function(tab) {
                app.tab = tab;
                if (tab === 'block-template') {
                    setTimeout(BTCHART.resize, 100);
                    setTimeout(BTCHART.resize, 500);
                    setTimeout(BTCHART.resize, 1000);
                }
                if (tab === 'dashboard') {
                    setTimeout(CHART.resize, 100);
                    setTimeout(CHART.resize, 500);
                    setTimeout(CHART.resize, 1000);
                }
                if (tab === 'network-chart') {
                    setTimeout(NETCHART.resize, 100);
                    setTimeout(NETCHART.resize, 500);
                    setTimeout(NETCHART.resize, 1000);
                }
            },
            rdBT: function() {
                if (app.blocktemplate === undefined) { return; }
                redrawBlockTemplate(app.blocktemplate);
            },
        },
    })

    setInterval(function() {
        app.now = getCurrentTime();
        if (((app.now - app.lastpong) > 10) && (app.connected)) {
            onDisconnect();
        }
    }, 1000);

    setInterval(function() {
        asyncRequest(`uptime`, processAsyncResponse);

        if (!app.connected) {
            return;
        }
        asyncRequest(`rest/chaininfo.json`, processAsyncResponse);
        asyncRequest(`peerinfo`, processAsyncResponse);
        asyncRequest("getmininginfo", dealWithMininginfo);
    }, 5000);

    setInterval(function() {
        if (!app.connected) {
            return;
        }

        asyncRequest("getwalletinfo", dealWithWalletinfo);
        asyncRequest("listreceivedbyaddress/0/true", dealWithRBA);
        asyncRequest("listtransactions/*/100", dealWithTransactions);
        asyncRequest(`blocktemplate`, processAsyncResponse);
    }, 15000);

    let bin_interval = 0;

    let clearBinInterval = function() {
        clearInterval(bin_interval);
        bin_interval = 0;
    }

    let setBinInterval = function(range) {
        if (bin_interval !== 0) {
            // should never happen
            return;
        }

        bin_interval = setInterval(function() {
            if (!app.connected) {
                return;
            }

            asyncRequest(`mempoolbins`, processAsyncResponse);
            asyncRequest(`nettotals`, processAsyncResponse);
        }, range*60000/120);

        app.range = range;
        app.last_updated = app.now; // makes next update appear cleanly

        asyncRequest(`mempoolbinsrange/${range}`, processAsyncResponse);
        asyncRequest(`nettotalsrange/${range}`, processAsyncResponse);
    }

    let dealWithSubmitPSBT = function(r, j) {
        if (j.result !== null) {
            app.wallet_send_address = null;
            app.wallet_createpsbt = null;
        }
        console.log(j);
    }

    let dealWithCreatePSBT = function(r, j) {
        // TODO: it should really be result, error
        //      so that we can nuke this on a bad
        app.wallet_createpsbt = j;
        let fee = j.fee;
        let changepos = j.changepos;
        for (let input of j.inputs) {
            // TODO does this assume segwit?
            let inamt = input.witness_utxo.amount;
            let inaddr = input.witness_utxo.scriptPubKey.address;
            console.log(inamt, inaddr);
        }
        for (let vout of j.tx.vout) {
            let outamt = vout.value;
            let outaddr = vout.scriptPubKey.addresses[0];
            if (vout.n == changepos) { console.log("CHANGE", outamt, outaddr); }
            else { console.log(outamt, outaddr); }
        }
    }

    let dealWithDebugOutput = function(r, j) {
        let jj = j.result;
        if (jj === null) {
            jj = j.error;
        }

        let stringified = JSON.stringify(jj, null, 4) + "\n";

        let output = r + "\n" + stringified;
        app.debug_output = app.debug_output + output;

        // hacky...
        setTimeout(function() {
            let el = document.getElementById("debug-console-output");
            el.scrollTop = el.scrollHeight;
        }, 250);
    }

    let dealWithChaininfo = function(chaininfo) {
        app.chaininfo = chaininfo;
        app.getBlockIfRequired(chaininfo.bestblockhash);
    };

    let dealWithMininginfo = function(r, mininginfo) {
        if (r != "getmininginfo") {
            return;
        }
        // TODO check if error
        app.mininginfo = mininginfo.result;
    };

    let dealWithWalletinfo = function(r, j) {
        if (j.error) {
            console.error(r, ":", j.error);
            return;
        }
        app.walletinfo = j.result;
    };

    let dealWithRBA = function(r, j) {
        if (j.error) {
            console.error(r, ":", j.error);
            return;
        }

        // extract some unused addresses
        let l = [];
        for (let o of j.result) {
            if (o.amount === 0 && o.txids.length === 0) {
                // unused
                l.push(o.address);
            }
        }

        // just use the first one
        l.sort();

        if (l.length > 0 && l[0] !== app.unused_address) {
            app.unused_address = l[0];
            let qrtext = `bitcoin:${l[0]}`;
            if (app.unused_address_qr === null) {
                app.unused_address_qr = new QRCode("waladdrqr", {
                    text: qrtext,
                    correctLevel: QRCode.CorrectLevel.H,
                });
                return;
            }
            app.unused_address_qr.clear();
            app.unused_address_qr.makeCode(qrtext);
            return;
        }

        if (l.length === 0) {
            // Get another address and look again
            // TODO should probably lock this down a bit server-side
            // e.g. only allow 10 outstanding max
            asyncRequest("getnewaddress", function(r, j) {
                // TODO check success
                console.log("getnewaddress...");
                console.log(j);
                asyncRequest("listreceivedbyaddress/0/true", dealWithRBA);
            });
        }
    }

    let dealWithTransactions = function(r, j) {
        if (j.error) {
            console.error(r, ":", j.error);
            return;
        }

        let t = j.result;

        t.sort(function(a, b) {
            if (a.timereceived < b.timereceived) { return -1; }
            if (a.timereceived > b.timereceived) { return 1; }
            return 0;
        }).reverse();

        app.transactions = t;
    }

    let dealWithGitStatus = function(r, t) {
        app.git_status = t;
    }

    let dealWithAuthorsMd = function(r, t) {
        app.authors_md = t;
    }

    let dealWithPeerinfo = function(peerinfo) {
        app.peers = peerinfo;
    }

    var BTCHART = null;

    let dealWithBlockTemplate = function(blocktemplate) {
        app.last_updated = app.now;
        app.blocktemplate = blocktemplate;

        redrawBlockTemplate(blocktemplate);
    }

    let redrawBlockTemplate = function(blocktemplate) {
        let cum_vsize = 0;
        let feerates = ["feerate sat/b"];
        let cum_vsizes = ["cum_vsize"];
        for (let tx of blocktemplate.tx) {
            let feerate = tx[0];
            let vsize = tx[1];

            // let depends = tx[2];
            if (app.bt_logscale) {
                feerates.push(Math.log(feerate)/Math.log(2));
            } else {
                feerates.push(feerate);
            }
            cum_vsizes.push(cum_vsize);
            cum_vsize += vsize;
        }

        let btcolumns = [
            cum_vsizes,
            feerates
        ];

        if (!BTCHART) {
            BTCHART = c3.generate({
                bindto: "#blocktemplatechart",
                transition: {
                    duration: 0
                },
                data: {
                    x: "cum_vsize",
                    columns: btcolumns,
                    type: "area",
                },
                point: {
                    show: false,
                },
                axis: {
                    x: {
                        tick: {
                            values: [0, 200000, 400000, 600000, 800000, 1000000],
                        }
                    },
                    y: {
                        tick: {
                            format: function (y) {
                                if (app.bt_logscale) {
                                    return Math.pow(2, y).toFixed(2);
                                }
                                return y.toFixed(2);
                            },
                        },
                    },
                },
                tooltip: {
                    format: {
                        value: function(y, ratio, id) {
                            if (app.bt_logscale) {
                                return `${Math.pow(2, y).toFixed(2)} sat/b`;
                            }
                            return `${y.toFixed(2)} sat/b`;
                        }
                    }
                }
            });
        } else {
            BTCHART.load({
                columns: btcolumns,
            });
        }
    }

    var CHART = null;
    var COLUMNS = null;

    let dealWithMempoolBins = function(mempoolbins, redraw=true) {
        app.last_updated = app.now;

        let getLabel = function(n) {
            if (n === ALL_BITCOINS) {
                return "max";
            }
            return `< ${mempoolbins.bins[i][1]} sat/b`;
        }

        let truncate = 120; // 120*15 = 1800s, half an hour.

        let utcdate = new Date(mempoolbins.time*1000);
        if (COLUMNS) {
            COLUMNS[0].push(utcdate);
        } else {
            COLUMNS = [ ["x", utcdate] ];
        }

        let excess_entries = (COLUMNS[0].length - truncate) - 1;
        if (excess_entries > 0) {
            COLUMNS[0] = COLUMNS[0].slice(excess_entries);
            COLUMNS[0][0] = "x";
        }

        let i = 0;
        let divisor = 0;
        if (app.data === 7) {
            divisor = 100000;
        } else if (app.data === 6) {
            divisor = 1048576;
        }
        for (let n of mempoolbins.bins.map(x => x[app.data])) {
            if ((COLUMNS.length - 2) < i) {
                let label = getLabel(mempoolbins.bins[i][1]);
                COLUMNS.push([label]);
            }

            if (divisor) {
                COLUMNS[i+1].push(n/divisor);
            } else {
                COLUMNS[i+1].push(n);
            }

            if (excess_entries > 0) {
                COLUMNS[i+1] = COLUMNS[i+1].slice(excess_entries);
                let label = getLabel(mempoolbins.bins[i][1]);
                COLUMNS[i+1][0] = label;
            };

            i++;
        };

        if (redraw) {
            if (!CHART) {
                CHART = c3.generate({
                    bindto: "#mempoolchart",
                    transition: {
                        duration: 0
                    },
                    data: {
                        x: "x",
                        columns: COLUMNS,
                    },
                    point: {
                        show: false,
                    },
                    axis: {
                        x: {
                            type: "timeseries",
                            padding: { left: 0, right: 0 },
                            tick: {
                                count: 7,
                                format: function (v) { return moment(v).format('MMM DD HH:mm'); },
                            },
                        },
                        y: {
                            min: 0,
                            padding: { bottom: 0 },
                        },
                    },
                    legend: {
                        position: "right",
                    },
                    grid: {
                        x: { show: true },
                        y: { show: true },
                    },
                    color: {
                        pattern: ["#203D54", "#203D54", "#203D54", "#1A4971", "#1A4971", "#1A4971", "#2368A2", "#2368A2", "#2368A2", "#3183C8", "#3183C8", "#3183C8", "#63A2D8", "#63A2D8", "#63A2D8", "#AAD4F5", "#AAD4F5", "#AAD4F5", "#EFF8FF", "#EFF8FF", "#EFF8FF"],
                    },
                    tooltip: {
                        format: {
                            value: function (value, ratio, id) {
                                if (app.data === 5) {
                                    return `${value} tx`;
                                } else if (app.data === 6) {
                                    return `${value.toFixed(3)} MiB`;
                                } else if (app.data === 7) {
                                    return `${value.toFixed(2)} mBTC`;
                                }
                            },
                        },
                    },
                });
            } else {
                CHART.load({
                    columns: COLUMNS,
                });
            }

            if ((app.mempoolbins === null) || (mempoolbins.time > app.mempoolbins.time)) {
                app.mempoolbins = mempoolbins;
            }
        }
    }

    let dealWithMempoolBinsRange = function(response) {
        for (let i = 0; i < response.list.length; i++) {
            let mempoolbins = response.list[i];
            if (i !== response.list.length-1) {
                dealWithMempoolBins(mempoolbins, redraw=false);
            } else {
                dealWithMempoolBins(mempoolbins);
            }
        }
    }

    let dealWithBlock = function(block) {
        Vue.set(app.blocks, block.hash, block);
        if (Object.keys(app.blocks).length < 12) {
            app.getBlockIfRequired(block.previousblockhash);
        }
    }

    let dealWithUptime = function(uptime) {
        if (!app.connected) {
            onConnect();
        }
        app.lastpong = app.now;
        // TODO: We assume it responded properly.
        // This seems reasonable to me. It's a fairly basic API call.
        app.uptime = uptime.result;
    }

    var NETCHART = null;
    var NETCOLUMNS = null;
    var NETLABELS = {
        "totalbytesrecv": "in / MiB",
        "totalbytessent": "out / MiB",
    }

    let dealWithNettotals = function(nettotals, redraw=true) {
        // app.last_updated = app.now;

        let truncate = 120; // 120*15 = 1800s, half an hour.

        let utcdate = new Date(nettotals.time*1000);
        if (NETCOLUMNS) {
            NETCOLUMNS[0].push(utcdate);
        } else {
            NETCOLUMNS = [ ["x", utcdate] ];
        }

        let excess_entries = (NETCOLUMNS[0].length - truncate) - 1;
        if (excess_entries > 0) {
            NETCOLUMNS[0] = NETCOLUMNS[0].slice(excess_entries);
            NETCOLUMNS[0][0] = "x";
        }

        let i = 0;
        for (let key of ["totalbytesrecv", "totalbytessent"]) {
            n = nettotals[key];

            if ((NETCOLUMNS.length - 2) < i) {
                NETCOLUMNS.push([NETLABELS[key]]);
            }

            NETCOLUMNS[i+1].push(n/1048576);

            if (excess_entries > 0) {
                NETCOLUMNS[i+1] = NETCOLUMNS[i+1].slice(excess_entries);
                NETCOLUMNS[i+1][0] = NETLABELS[key];
            };

            i++;
        };

        if (redraw) {
            if (!NETCHART) {
                NETCHART = c3.generate({
                    bindto: "#networkchart",
                    transition: {
                        duration: 0
                    },
                    data: {
                        x: "x",
                        columns: NETCOLUMNS,
                    },
                    point: {
                        show: false,
                    },
                    axis: {
                        x: {
                            type: "timeseries",
                            padding: { left: 0, right: 0 },
                            tick: {
                                count: 7,
                                format: function (v) { return moment(v).format('MMM DD HH:mm'); },
                            },
                        },
                        y: {
                            min: 0,
                            padding: { bottom: 0 },
                        },
                    },
                    legend: {
                        position: "right",
                    },
                    grid: {
                        x: { show: true },
                        y: { show: true },
                    },
                    color: {
                        pattern: ["darkgreen", "crimson"],
                    },
                });
            } else {
                NETCHART.load({
                    columns: NETCOLUMNS,
                });
            }

            app.nettotals = nettotals;
        }
    }

    let dealWithNettotalsRange = function(response) {
        for (let i = 0; i < response.list.length; i++) {
            let nettotals = response.list[i];
            if (i !== response.list.length-1) {
                dealWithNettotals(nettotals, redraw=false);
            } else {
                dealWithNettotals(nettotals);
            }
        }
    }

    let processAsyncResponse = function(request, response) {
        if (request.startsWith("uptime")) {
            dealWithUptime(response);
        } else if (request.startsWith("rest/chaininfo.json")) {
            dealWithChaininfo(response);
        } else if (request.startsWith("peerinfo")) {
            dealWithPeerinfo(response);
        } else if (request.startsWith("blockinfo")) {
            dealWithBlock(response);
        } else if (request.startsWith("blocktemplate")) {
            dealWithBlockTemplate(response);
        } else if (request.startsWith("mempoolbinsrange")) {
            dealWithMempoolBinsRange(response);
        } else if (request.startsWith("mempoolbins")) {
            dealWithMempoolBins(response);
        } else if (request.startsWith("nettotalsrange")) {
            dealWithNettotalsRange(response);
        } else if (request.startsWith("nettotals")) {
            dealWithNettotals(response);
        }
    }

    let onConnect = function() {
        app.connected = true;
        asyncRequest(`rest/chaininfo.json`, processAsyncResponse);
        asyncRequest(`peerinfo`, processAsyncResponse);
        asyncRequest(`blocktemplate`, processAsyncResponse);
        setBinInterval(INITIAL_RANGE); // implicit fetch of mempoolbinsrange, nettotalsrange
        asyncRequest(`mempoolbins`, processAsyncResponse);
        // fetch latest mempoolbins regardless of our interval
        asyncRequest("getmininginfo", dealWithMininginfo);
        asyncRequest("getwalletinfo", dealWithWalletinfo);

        asyncRequest("listreceivedbyaddress/0/true", dealWithRBA);
        asyncRequest("listtransactions/*/100", dealWithTransactions);
    }

    let onDisconnect = function() {
        app.connected = false;
    }

    asyncRequest(`uptime`, processAsyncResponse);
    rawAsyncRequest(`${THIS_URL}/_git_status`, dealWithGitStatus);
    rawAsyncRequest(`${THIS_URL}/AUTHORS.md`, dealWithAuthorsMd);

    let mod = function(a, b) {
        return ((a % b) + b) % b;
    }

    let processQrCode = function(a) {
        app.toggleScanner();
        if (a.startsWith("bitcoin:")) {
            a = a.replace("bitcoin:","");
        }
        app.wallet_send_address = a;
        app.submitCreatePSBT();
    }

    document.onkeydown = function(e) {
        if ((e.key == "ArrowDown") || (e.key === "ArrowUp")) {
            let i;
            for (i = 0; i < i < TABS.length; i++) {
                if (TABS[i][0] == app.tab) {
                    break;
                }
            }

            if (e.key === "ArrowDown") { // ArrowDown
                i++;
            } else { // ArrowUp
                i--;
            }

            i = mod(i, TABS.length);
            app.tab = TABS[i][0];

            e.preventDefault();
        }
    }

    var SCANNER = new Instascan.Scanner({ video: document.getElementById("qrscan") });
    SCANNER.addListener('scan', processQrCode);
    var SCANNER_ENABLED = false;
}
