#!/bin/bash

#
# Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
# Electron Relocation Limited
#
# This file is part of bitcoin-top.  bitcoin-top is free software: you can
# redistribute it and/or modify it under the terms of the GNU Affero General
# Public License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
#

>&2 echo "Building..."

>&2 echo "Generating _git_status..."

echo "bitcoin-top" > nginx/public_html/_git_status
git rev-parse HEAD >> nginx/public_html/_git_status
git log -1 --oneline HEAD >> nginx/public_html/_git_status
git status --porcelain >> nginx/public_html/_git_status
cp nginx/public_html/_git_status nginx/public_html/_git_status.txt

cp AUTHORS.md nginx/public_html/AUTHORS.md
cp AUTHORS.md nginx/public_html/AUTHORS.md.txt

# If the build step fails, try uncommenting this:
NOCACHE=
#NOCACHE=--no-cache
sudo docker-compose build $NOCACHE $@
rm nginx/public_html/_git_status
rm nginx/public_html/_git_status.txt
rm nginx/public_html/AUTHORS.md
rm nginx/public_html/AUTHORS.md.txt

>&2 echo "Running..."
sudo docker-compose up -d --remove-orphans && sudo docker-compose logs -f --tail="10"
