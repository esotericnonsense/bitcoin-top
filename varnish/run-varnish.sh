#!/bin/sh

#
# Copyright (C) 2016-2018 Daniel Edgecumbe (esotericnonsense)
# Electron Relocation Limited
#
# This file is part of bitcoin-top.  bitcoin-top is free software: you can
# redistribute it and/or modify it under the terms of the GNU Affero General
# Public License as published by the Free Software Foundation, either version 3
# of the License, or (at your option) any later version.
#
# bitcoin-top is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License
# along with bitcoin-top.  If not, see <http://www.gnu.org/licenses/>.
#

HOSTNAME=$(hostname)
mkdir -p /var/lib/varnish/$HOSTNAME
chown nobody /var/lib/varnish/$HOSTNAME
varnishd -F -s malloc,100M -a :80 -b nginx:80
